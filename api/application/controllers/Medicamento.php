<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	require_once(BASEPATH.'../application/objects/Medicamento_object.php');
	require_once(BASEPATH.'../application/objects/Paciente_medicamento_object.php');
	

	class Medicamento extends Base_Controller
	{
		public $my_dao = "medicamento_model";


		public function __construct()
		{
			parent::__construct();
		}	

    	public function list_all(){
	       try{
		       
		        $result = $this->get_my_dao()->get_all();	
		        $this->to_json_response($result);
		    }catch(Exception $e){
		        $this->to_json_exception($e);
		    }
    	}

    	public function getById($id){
	       try{
		       
		        $result = $this->get_my_dao()->get_by_id($id);	
		        $this->to_json_response($result);
		    }catch(Exception $e){
		        $this->to_json_exception($e);
		    }
    	}



    	public function save(){
	        try{  
	          
	            $medicamento_object = new Medicamento_object();
	            $medicamento_object->set_from_post_to_create();	           
	            $response = $this->get_my_dao()->save($medicamento_object, 'medicamentoid'); 
	                                   
		        

	        }catch(Exception $e){
	            $this->to_json_exception($e);
	        }
    	}   

    	public function savePacienteMedicamento(){
	        try{  
	          	$this->load->model("paciente_medicamento_model");
	            $paciente_medicamento_object = new Paciente_medicamento_object();
	            $paciente_medicamento_object->set_from_post_to_create();	    
	            $response = $this->paciente_medicamento_model->save($paciente_medicamento_object, 'id'); 
	                                   
		       

	        }catch(Exception $e){
	            $this->to_json_exception($e);
	        }
    	} 

    	public function deletar(){
    		
    		$this->load->model("paciente_medicamento_model");
	        try{  
	            //$this->auth();
	            $jsonObject = file_get_contents('php://input'); 
			    $jsonObject = json_decode($jsonObject);	  
	            
	            $this->paciente_medicamento_model->delete($jsonObject);
	            $paciente_obj = $this->get_my_dao()->deletar($jsonObject); 
	          
				
	        	
			          
	        }catch(Exception $e){
	            $this->to_json_exception($e);
	        }
    	}     

    	

	}