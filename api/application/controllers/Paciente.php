<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	require_once(BASEPATH.'../application/objects/Paciente_object.php');
	

	class Paciente extends Base_Controller
	{
		public $my_dao = "paciente_model";


		public function __construct()
		{
			parent::__construct();
		}	

    	public function list_all(){
	       try{
		        //$this->auth();
		        $result = $this->get_my_dao()->get_all();	
		        $this->to_json_response($result);
		    }catch(Exception $e){
		        $this->to_json_exception($e);
		    }
    	}

    	public function getPacienteById($id){
	       try{
		        $result = $this->get_my_dao()->get_paciente_by_id($id);	
		        $this->to_json_response($result);
		    }catch(Exception $e){
		        $this->to_json_exception($e);
		    }
    	}

    	public function getMedicamentoByPaciente($id){
	       try{
		        $result = $this->get_my_dao()->get_medicamento_by_paciente($id);	
		        $this->to_json_response($result);
		    }catch(Exception $e){
		        $this->to_json_exception($e);
		    }
    	}

    	public function save(){
	        try{  
	           // $this->auth();  
	            $paciente_object = new Paciente_object();
	            $paciente_object->set_from_post_to_create();	           
	            $response = $this->get_my_dao()->save($paciente_object, 'pacienteid'); 
	                                   
		      

	        }catch(Exception $e){
	            $this->to_json_exception($e);
	        }
    	}   

    	public function deletar(){
    		
    		$this->load->model("cliente_paciente_model");
	        try{  
	            //$this->auth();
	            $jsonObject = file_get_contents('php://input'); 
			    $jsonObject = json_decode($jsonObject);	  
	            
	            $this->cliente_paciente_model->deletepaciente($jsonObject);
	            $paciente_obj = $this->get_my_dao()->deletar($jsonObject); 
	            //$this->cliente_paciente_model->delete_cliente_by_paciente($jsonObject);
				
	        	
			          
	        }catch(Exception $e){
	            $this->to_json_exception($e);
	        }
    	}   

    	

	}