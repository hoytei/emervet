<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	require_once(BASEPATH.'../application/objects/Cliente_object.php');
	require_once(BASEPATH.'../application/objects/Paciente_object.php');
	require_once(BASEPATH.'../application/objects/Cliente_paciente_object.php');


	

	class Cliente extends Base_Controller
	{
		public $my_dao = "cliente_model";
		


		public function __construct()
		{
			parent::__construct();
		}	

    	public function list_all(){
	       try{
		       // $this->auth();
		        $result = $this->get_my_dao()->get_all();	
		        $this->to_json_response($result);
		    }catch(Exception $e){
		        $this->to_json_exception($e);
		    }
    	}

    	public function getPacienteByCliente($id){
	       try{
		        $result = $this->get_my_dao()->get_paciente_by_cliente($id);	
		        $this->to_json_response($result);
		    }catch(Exception $e){
		        $this->to_json_exception($e);
		    }
    	}

    	public function getClienteById($id){
	       try{
		        $result = $this->get_my_dao()->get_cliente_by_id($id);	
		        $this->to_json_response($result);
		    }catch(Exception $e){
		        $this->to_json_exception($e);
		    }
    	}
 
    	public function save(){
    		$this->load->model("paciente_model");
    		$this->load->model("cliente_paciente_model");
	        try{  
	            //$this->auth();  
	            $cliente_object = new Cliente_object();
	            $cliente_object->set_from_post_to_create();	
	            $paciente_obj = $this->get_my_dao()->save($cliente_object, 'clienteid'); 
				if(isset($cliente_object->pacientes)){
		            foreach ($paciente_obj->getPacientesList() as $paciente) { 
		            	$paciente_object = new Paciente_object($paciente);
		            	$paciente_object = $this->paciente_model->save($paciente_object, 'pacienteid');
		            	//$paciente_object->set_from_post_to_create();
		            	$cp = $this->cliente_paciente_model->get_paciente_by_cliente_paciente($paciente_object->get_id(), $cliente_object->get_id());
		            	
		            	if($cp==null){ 
		            		$cpobj = new stdClass;
		            		$cpobj->clienteid = $cliente_object->get_id();
		            		$cpobj->pacienteid = $paciente_object->get_id();
		            		$obj_cp = new Cliente_paciente_object($cpobj);
		            		$this->cliente_paciente_model->save($obj_cp, 'id');	            	
		            	}          	
		            	
		            }
	        	}
	            $this->to_json_response($cliente_object->get_id());
			          
	        }catch(Exception $e){
	            $this->to_json_exception($e);
	        }
    	}   

    	public function deletar(){
    		$this->load->model("paciente_model");
    		$this->load->model("cliente_paciente_model");
	        try{  
	            //$this->auth();
	            $jsonObject = file_get_contents('php://input'); 
			    $jsonObject = json_decode($jsonObject);	  
	            
	            $this->cliente_paciente_model->deletecliente($jsonObject);
	            $paciente_obj = $this->get_my_dao()->deletar($jsonObject); 
	            $this->cliente_paciente_model->delete_paciente_by_cliente($jsonObject);
				
	        	
			          
	        }catch(Exception $e){
	            $this->to_json_exception($e);
	        }
    	}   

    	public function savePacienteByCliente($clienteid){
    		$this->load->model("paciente_model");
    		$this->load->model("cliente_paciente_model");
	        try{  
	           
	            	$paciente_object = new Paciente_object();	            	
	            	$paciente_object->set_from_post_to_create();
	            	$paciente_object = $this->paciente_model->save($paciente_object, 'pacienteid');	            	
	            	
	            	$cpobj = new stdClass;
	            	$cpobj->clienteid = $clienteid;
	            	$cpobj->pacienteid = $paciente_object->get_id();
	            	$obj_cp = new Cliente_paciente_object($cpobj);
	            	$this->cliente_paciente_model->save($obj_cp, 'id');	            	
	            	          	
	            	
	            
	            
	        }catch(Exception $e){
	            $this->to_json_exception($e);
	        }
    	}   


    	

	}