<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	
	function load_mysql_functions($db){
		if ($db->dbdriver == "sqlite3") {
			
			//Bug do database locked
			//referencia: 
			//If you get the error message saying "SQLite3::exec. database locked." You just need to define a busyTimeout to work around this.
			//http://php.net/manual/en/sqlite3.exec.php
			$db->conn_id->busyTimeout(10000);

			$db->conn_id->createFunction('DATE_FORMAT', 'dateFormatMysql');
			$db->conn_id->createFunction('now', 'nowMysql');
			$db->conn_id->createFunction('MINUTE', 'minuteMysql');
			$db->conn_id->createFunction('concat', 'concatMysql');
			
		}
	}

	function dateFormatMysql($value, $format){
		
		$date = new DateTime($value);
		$format = str_replace("%", "", $format);
		return $date->format($format);
	}

	function nowMysql(){
		$newTZ = new DateTimeZone("America/Sao_Paulo");
		$date  = new DateTime(date("Y-m-d H:i:s"));
		$date->setTimezone( $newTZ );
		$data = $date->format('Y-m-d');
		$time = $date->format('H:i:s');

		return $data." ".$time;
	}

	function concatMysql($val1, $val2){
		return $val1.$val2;
	}

	function minuteMysql(){
		//TODO
	}


	


	
