<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  require_once(BASEPATH.'../application/objects/Base_object.php');


  class Paciente_medicamento_object extends Base_object
  {
		private $pacienteid;
		private $medicamentoid;
		protected $id;
		private $doseutilizada;
		

		
		public function __construct($row=null){

			parent::__construct($row);

			if (isset($row)) {				
				           
				$this->pacienteid = $row->pacienteid;				
				$this->medicamentoid = $row->medicamentoid;	
				$this->doseutilizada = $row->doseutilizada;			
				if(isset($row->id))
					$this->id = $row->id;			
			}
			
		}

		 public function set_from_post_to_create(){
            $request = parent::getJsonRequest(); 
               
         	if(isset($request->id))
            	$this->set_id($request->id);
            $this->set_pacienteid($request->pacienteid);
            $this->set_medicamentoid($request->medicamentoid);
            $this->set_dose($request->doseutilizada);
                     
           
        }

		

        public function set_id($id){
        	$this->id = $id;
        }

        public function get_id(){
        	return $this->id;
        }

         public function set_dose($doseutilizada){
        	$this->doseutilizada = $doseutilizada;
        }

        public function get_dose(){
        	return $this->doseutilizada;
        }

 
		public function set_medicamentoid($medicamentoid){
			$this->medicamentoid = $medicamentoid;
		}
		public function get_medicamentoid(){
			return $this->medicamentoid;
		}

		public function set_pacienteid($pacienteid){
			$this->pacienteid = $pacienteid;
		}

		public function get_pacienteid($pacienteid){
			return $this->pacienteid;
		}
		


		public function to_array(){ 
			//$data 			= parent::to_array();
			$data["medicamentoid"]	   = $this->medicamentoid;
			$data["pacienteid"]   = $this->pacienteid;
			$data["doseutilizada"] = $this->doseutilizada;
			$data["id"] = $this->get_id();
			
			return $data;	
		}
		
		
		
  }


