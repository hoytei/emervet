<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  require_once(BASEPATH.'../application/objects/Base_object.php');
  //require_once(BASEPATH.'../application/objects/Establishment_object.php'); 

  class Cliente_object extends Base_object
  {
		private $nome;
		private $pacienteArray;
		private $cpf;
		private $telefone;
		private $endereco;
		protected $clienteid;

		
		public function __construct($row=null){

			parent::__construct($row);

			if (isset($row)) {				
				           
				$this->nome = $row->nome;				
				$this->cpf = $row->cpf;
				$this->telefone = $row->telefone;
				$this->endereco = $row->endereco;	
				$this->id = $row->clienteid;			
			}
			
		}

		 public function set_from_post_to_create(){
            $request = parent::getJsonRequest(); 
                
          if(isset($request->pacientes)){	              
	           $this->setPacientesList($request->pacientes);                 
           }
           if(isset($request->clienteid))
           	   $this->set_id($request->clienteid);

            $this->set_nome($request->nome);
            $this->set_cpf($request->cpf);
            $this->set_telefone($request->telefone);
            $this->set_endereco($request->endereco); 

           
                          
           
        }

        public function set_id($id){
        	$this->clienteid = $id;
        }

        public function get_id(){
        	return $this->clienteid;
        }

 
		public function set_nome($nome){
			$this->nome = $nome;
		}
		public function get_name(){
			return $this->name;
		}

		public function set_cpf($cpf){
			$this->cpf = $cpf;
		}
		public function get_cpf(){
			return $this->cpf;
		}

		public function set_telefone($telefone){
			$this->telefone = $telefone;
		}
		public function get_telefone(){
			return $this->telefone;
		}

		public function set_endereco($endereco){
			$this->endereco = $endereco;
		}
		public function get_endereco(){
			return $this->endereco;
		}

		public function setPacientesList($pacientes){
			$this->pacientes = $pacientes;
		}
		public function getPacientesList(){
			return $this->pacientes;
		}
		 


		public function to_array(){ 
			//$data 			= parent::to_array();
			$data["clienteid"]	   = $this->get_id();
			$data["nome"]   = $this->nome;
			$data["cpf"] = $this->cpf;
			$data["telefone"] = $this->telefone;
			$data["endereco"] = $this->endereco;
			return $data;	
		}
		
		
		public function to_json(){			
			$json 			= new StdClass();
			$json->id       = $this->id;
			$json->nome     = $this->nome;
			$json->cpf   = $this->cpf;
			$json->telefone   = $this->telefone;
			$json->endereco   = $this->endereco;
			  
			return $json;
		}
  }


