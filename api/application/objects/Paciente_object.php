<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  require_once(BASEPATH.'../application/objects/Base_object.php');
  //require_once(BASEPATH.'../application/objects/Establishment_object.php'); 

  class Paciente_object extends Base_object
  {
		private $nome;
		private $peso;
		private $especie;
		protected $id;
		

		
		public function __construct($row=null){

			parent::__construct($row);

			if (isset($row)) {				
				           
				$this->nome = $row->nome;				
				$this->peso = $row->peso;
				$this->especie = $row->especie;
				if(isset($row->pacienteid))
					$this->id = $row->pacienteid;				
			}
			
		}

		 public function set_from_post_to_create(){
            $request = parent::getJsonRequest(); 
               
          if(isset($request->medicamentos)){	              
	           $this->setMedicamentoList($request->medicamentos);                 
           }

           if(isset($request->pacienteid))
            	$this->set_id($request->pacienteid);
            
            $this->set_nome($request->nome);
            $this->set_peso($request->peso);
            $this->set_especie($request->especie);
                     
           
        }

        public function set_id($id){
        	$this->id = $id;
        }

        public function get_id(){
        	return $this->id;
        }

 
		public function set_nome($nome){
			$this->nome = $nome;
		}
		public function get_name(){
			return $this->name;
		}

		public function set_peso($peso){
			$this->peso = $peso;
		}
		public function get_cpf(){
			return $peso->peso;
		}

		public function set_especie($especie){
			$this->especie = $especie;
		}
		public function get_especie(){
			return $especie->especie;
		}

		
		public function setMedicamentoList($medicamentos){
			$this->medicamentos = $medicamentos;
		}
		public function getMedicamentoList(){
			return $medicamentos->medicamentos;
		}
		 


		public function to_array(){ 
			//$data 			= parent::to_array();
			$data["pacienteid"]	   = $this->get_id();
			$data["nome"]   = $this->nome;
			$data["peso"] = $this->peso;
			$data["especie"] = $this->especie;
			return $data;	
		}
		
		
		public function to_json(){			
			$json 			= new StdClass();
			$json->id       = $this->id;
			$json->nome     = $this->nome;
			$json->peso   = $this->peso;
			$json->especie   = $this->especie;
			  
			return $json;
		}
  }


