<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  require_once(BASEPATH.'../application/objects/Base_object.php');
  //require_once(BASEPATH.'../application/objects/Establishment_object.php'); 

  class Medicamento_object extends Base_object
  {
		private $nome;
		private $medicamentoid;
		private $nomecomercial;
		private $concentracao;
		private $dosecao;
		private $dosegato;
		
		

		
		public function __construct($row=null){

			parent::__construct($row);

			if (isset($row)) {				
				           
				$this->nome = $row->nome;
				if(isset($this->medicamentoid))	
					$this->medicamentoid = $row->medicamentoid;
				$this->nomecomercial = $row->nomecomercial;
				$this->concentracao = $row->concentracao;
				$this->dosegato = $row->dosegato;
				$this->dosecao = $row->dosecao;
					
							
			}
			
		}

		 public function set_from_post_to_create(){
            $request = parent::getJsonRequest(); 
            
            if(isset($request->MedicamentoID))    
           		$this->set_id($request->MedicamentoID);
            $this->set_nome($request->Nome);
            $this->set_nomecomercial($request->Nomecomercial);
            $this->set_concentracao($request->Concentracao);
            $this->set_dosecao($request->DoseCao);
            $this->set_dosegato($request->DoseGato);
           
                     
           
        }

        public function set_id($medicamentoid){
        	$this->medicamentoid = $medicamentoid;
        }

        public function get_id(){
        	return $this->medicamentoid;
        }

 
		public function set_nome($nome){
			$this->nome = $nome;
		}
		public function get_nome(){
			return $this->nome;
		}

		public function set_nomecomercial($nomecomercial){
			$this->nomecomercial = $nomecomercial;
		}
		public function get_nomecomercial(){
			return $this->nomecomercial;
		}

		public function set_concentracao($concentracao){
			$this->concentracao = $concentracao;
		}
		public function get_concentracao(){
			return $concentracao->concentracao;
		}
		
		public function set_dosecao($dosecao){
			$this->dosecao = $dosecao;
		}
		public function get_dosecao(){
			return $this->dosecao;
		}
		 
		public function set_dosegato($dosegato){
			$this->dosegato = $dosegato;
		}
		public function get_dosegato(){
			return $this->dosegato;
		}

		


		public function to_array(){ 
			//$data 			= parent::to_array();
			$data["medicamentoid"]	   = $this->get_id();
			$data["nome"]   = $this->nome;
			$data["nomecomercial"] = $this->nomecomercial;
			$data["concentracao"] = $this->concentracao;
			$data["dosecao"] = $this->dosecao;
			$data["dosegato"] = $this->dosegato;
			
			return $data;	
		}
		
		
		public function to_json(){			
			$json 			= new StdClass();
			$json->medicamentoid       = $this->medicamentoid;
			$json->nome     = $this->nome;
			$json->nomecomercial   = $this->nomecomercial;
			$json->concentracao  = $this->concentracao;
			$json->dosecao  = $this->dosecao;
			$json->dosegato  = $this->dosegato;
		
			  
			return $json;
		}
  }


