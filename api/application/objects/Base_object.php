<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


  class Base_object
  {
		protected $id;
		

		public function __construct($active_record=null){
			if (isset($active_record)) {
				if(isset($active_record->id)) {
					$this->id   = $active_record->id;
				}
				
			}
		}

		
		public function array_objects_to_json($array){
			$res = array();
			$idx = 0;
			foreach ($array as $item) {
				$res[$idx] = $item->to_json();
				$idx++;
			}
      		return $res;
		}

			
		

		protected function getJsonRequest(){
			$jsonObject = file_get_contents('php://input'); 
			$jsonObject = json_decode($jsonObject);			
            if (!isset($jsonObject)) {
                throw new Exception("This method must be request with 'application/json' header and a valid JSON in the content");
            }
            return $jsonObject;
		}
  }


