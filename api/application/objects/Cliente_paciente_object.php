<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  require_once(BASEPATH.'../application/objects/Base_object.php');
  //require_once(BASEPATH.'../application/objects/Establishment_object.php'); 

  class Cliente_paciente_object extends Base_object
  {
		private $pacienteid;
		private $clienteid;
		protected $id;
		

		
		public function __construct($row=null){

			parent::__construct($row);

			if (isset($row)) {				
				           
				$this->pacienteid = $row->pacienteid;				
				$this->clienteid = $row->clienteid;				
				if(isset($row->id))
					$this->id = $row->id;			
			}
			
		}

		

        public function set_id($id){
        	$this->id = $id;
        }

        public function get_id(){
        	return $this->id;
        }

 
		public function set_clienteid($clienteid){
			$this->clienteid = $clienteid;
		}
		public function get_clienteid(){
			return $this->clienteid;
		}

		public function set_pacienteid($pacienteid){
			$this->pacienteid = $pacienteid;
		}

		public function get_pacienteid($pacienteid){
			return $this->pacienteid;
		}
		


		public function to_array(){ 
			//$data 			= parent::to_array();
			$data["clienteid"]	   = $this->clienteid;
			$data["pacienteid"]   = $this->pacienteid;
			$data["id"] = $this->get_id();
			
			return $data;	
		}
		
		
		
  }


