<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
  //require_once(BASEPATH.'../application/objects/Category_object.php');

  class Paciente_model extends Base_Model
  {
      protected $_table = 'paciente'; 

      // $this->db->where("mi.establishment_id", $establishment_id);      

      public function get_all(){            
          $this->db->select("p.pacienteid, p.nome, especie, peso, c.nome as nome_cliente");
          $this->db->from("paciente p");      
          $this->db->join("clientepaciente cp", "cp.pacienteid = p.pacienteid");
          $this->db->join("cliente c", "cp.clienteid = c.clienteid");
          $query  = $this->db->get();
         
          return $query->result();    
      }

       public function get_paciente_by_id($id){            
          $this->db->select("pacienteid, nome, peso, especie");
          $this->db->from("paciente");      
          $this->db->where("pacienteid", $id);  
          $query  = $this->db->get();
         
          return $query->result();    
      }

      public function get_medicamento_by_paciente($id){            
          $this->db->select("nomecomercial as nome, data, doseutilizada");
          $this->db->from("medicamento m");
          $this->db->join("pacientemedicamento pm", "m.medicamentoid = pm.medicamentoid"); 
          $this->db->where("pm.pacienteid", $id);            
          $query  = $this->db->get();
         
          return $query->result();        
      }

      public function deletar($id){            
          
          $this->db->where("pacienteid", $id); 
          $this->db->delete("paciente");  

       
         
                
      }

      
  }


