<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
  //require_once(BASEPATH.'../application/objects/Category_object.php');

  class Medicamento_model extends Base_Model
  {
      protected $_table = 'medicamento'; 

      // $this->db->where("mi.establishment_id", $establishment_id);      

      public function get_all(){            
          $this->db->select("*");
          $this->db->from("medicamento");      
          $this->db->order_by("nomecomercial");
          $query  = $this->db->get();
         
          return $query->result();    
      }

       public function get_by_id($id){            
          $this->db->select("*");
          $this->db->from("medicamento");      
          $this->db->where("medicamentoid", $id);  
          $query  = $this->db->get();
         
          return $query->result();    
      }

      public function get_medicamento_by_paciente($id){            
          $this->db->select("nomecomercial as nome, data, doseutilizada");
          $this->db->from("medicamento m");
          $this->db->join("pacientemedicamento pm", "m.medicamentoid = pm.medicamentoid"); 
          $this->db->where("pm.pacienteid", $id);            
          $query  = $this->db->get();
         
          return $query->result();        
      }

      public function deletar($id){            
          
          $this->db->where("medicamentoid", $id); 
          $this->db->delete("medicamento");       
         
                
      }


      
  }


