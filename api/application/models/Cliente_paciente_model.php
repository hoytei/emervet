<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
  //require_once(BASEPATH.'../application/objects/Category_object.php');

  class Cliente_paciente_model extends Base_Model
  {
      protected $_table = 'clientepaciente'; 

       public function get_paciente_by_cliente_paciente($pacienteid, $clienteid){   
          $this->db->from("clientepaciente");
          $this->db->where("clienteid", $clienteid); 
          $this->db->where("pacienteid", $pacienteid);            
          $query  = $this->db->get();
         
          return $query->result();     
        }

         public function deletecliente($id){            
          
          $this->db->where("clienteid", $id); 
          $this->db->delete("clientepaciente");            
                
      }

      public function delete_paciente_by_cliente($clienteid){
          $this->db->select("pacienteid");
          $this->db->from("clientepaciente");
          $this->db->where("clienteid", $clienteid); 
                   
          $query  = $this->db->get(); 

          foreach ($query->result() as $pacienteid) { 
               $this->db->where('pacienteid', $pacienteid->pacienteid);   
               $this->db->delete("paciente");       
          }

      }



      public function deletepaciente($id){            
          
          $this->db->where("pacienteid", $id); 
          $this->db->delete("clientepaciente");  
       
      }
      
  }


