<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
  //require_once(BASEPATH.'../application/objects/Category_object.php');

  class Cliente_model extends Base_Model
  {
      protected $_table = 'cliente'; 

      // $this->db->where("mi.establishment_id", $establishment_id);      

      public function get_all(){            
          $this->db->select("clienteid, nome, endereco, cpf, telefone");
          $this->db->from("cliente");      
          $query  = $this->db->get();
         
          return $query->result();    
      }

       public function get_cliente_by_id($id){            
          $this->db->select("clienteid, nome, endereco, cpf, telefone");
          $this->db->from("cliente");      
          $this->db->where("clienteid", $id);  
          $query  = $this->db->get();
         
          return $query->result();    
      }

      public function get_paciente_by_cliente($id){            
          $this->db->select("p.pacienteid, nome, especie, peso");
          $this->db->from("paciente p");
          $this->db->join("clientepaciente cp", "cp.pacienteid = p.pacienteid"); 
          $this->db->where("cp.clienteid", $id);            
          $query  = $this->db->get();
         
          return $query->result();        
      }

      public function deletar($id){            
          
          $this->db->where("clienteid", $id); 
          $this->db->delete("cliente");  

       
         
                
      }


      
  }


