(function () {
    'use strict';
    angular.module('app')
        .factory('PacienteService', ['$http', function ($http) {
        
            function PacienteService($scope) {
                this.http = $http;
                this.scope = $scope;
            }            

            PacienteService.prototype.getPacientes = function (callback) { 
                var url = "http://localhost/emervet/api/index.php/paciente/list_all/";         
                $http.get(url,{cache: false}).success(function(response) {
                    callback(response);
                });      
            };                       
            
            PacienteService.prototype.getPacienteById = function (id, callback) { 
                var url = "http://localhost/emervet/api/index.php/paciente/getPacienteById/" +id;         
                $http.get(url,{cache: false}).success(function(response) {
                    callback(response);
                });      
            }; 

            PacienteService.prototype.getMedicamentoByPaciente = function (id, callback) { 
                var url = "http://localhost/emervet/api/index.php/paciente/getMedicamentoByPaciente/" +id;         
                $http.get(url,{cache: false}).success(function(response) {
                    callback(response);
                });      
            };   

             PacienteService.prototype.deletar= function (json, callback) { 
                var url = "http://localhost/emervet/api/index.php/paciente/deletar/";            
                var contentType = {'Content-Type': 'application/json'};
                $http.post(url, json,{cache: false}).then(function(response) {
                    callback(response);
                });               
            };

            PacienteService.prototype.save= function (json, callback) {
                var url = "http://localhost/emervet/api/index.php/paciente/save/";            
                var contentType = {'Content-Type': 'application/json'};
                $http.post(url, json,{cache: false}).then(function(response) {
                    callback(response);
                });               
            }; 
            return PacienteService;
        }])

}) (); 


