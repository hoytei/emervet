(function () {
    'use strict';
    angular.module('app')
        .factory('MedicamentoService', ['$http', function ($http) {
        
            function MedicamentoService($scope) {
                this.http = $http;
                this.scope = $scope;
            }            

            MedicamentoService.prototype.getMedicamentos = function (callback) { 
                var url = "http://localhost/emervet/api/index.php/medicamento/list_all/";         
                $http.get(url,{cache: false}).success(function(response) {
                    callback(response);
                });      
            };                       
            
             MedicamentoService.prototype.getMedicamentoById = function (id, callback) { 
                var url = "http://localhost/emervet/api/index.php/medicamento/getById/" +id;         
                $http.get(url,{cache: false}).success(function(response) {
                    callback(response);
                });      
            };   

            MedicamentoService.prototype.save= function (json, callback) {
                var url = "http://localhost/emervet/api/index.php/medicamento/save";            
                var contentType = {'Content-Type': 'application/json'};
                $http.post(url, json,{cache: false}).then(function(response) {
                    callback(response);
                });               
            };

            MedicamentoService.prototype.deletar= function (json, callback) { 
                var url = "http://localhost/emervet/api/index.php/medicamento/deletar/";            
                var contentType = {'Content-Type': 'application/json'};
                $http.post(url, json,{cache: false}).then(function(response) {
                    callback(response);
                });               
            };

            MedicamentoService.prototype.savePacienteMedicamento= function (json, callback) {
                var url = "http://localhost/emervet/api/index.php/medicamento/savePacienteMedicamento";            
                var contentType = {'Content-Type': 'application/json'};
                $http.post(url, json,{cache: false}).then(function(response) {
                    callback(response);
                });               
            };  

            return MedicamentoService;
        }])

}) (); 


