(function () {
    'use strict';
    angular.module('app')
        .factory('ClienteService', ['$http', function ($http) {
        
            function ClienteService($scope) { 
                this.http = $http;
                this.scope = $scope;
            }            

            ClienteService.prototype.getClientes = function (callback) { 
                var url = "http://localhost/emervet/api/index.php/cliente/list_all/"; 

                $http.get(url,{cache: false}).success(function(response) {
                    callback(response);
                });      
            };                       
            
             ClienteService.prototype.getClienteById = function (id, callback) { 
                var url = "http://localhost/emervet/api/index.php/cliente/getClienteById/" +id;         
                $http.get(url,{cache: false}).success(function(response) {
                    callback(response);
                });      
            };   

            ClienteService.prototype.getPacienteByCliente = function (id, callback) { 
                var url = "http://localhost/emervet/api/index.php/cliente/getPacienteByCliente/" +id;         
                $http.get(url,{cache: false}).success(function(response) {
                    callback(response);
                });      
            };   

            ClienteService.prototype.save= function (json, callback) {
                var url = "http://localhost/emervet/api/index.php/cliente/save/";            
                var contentType = {'Content-Type': 'application/json'};
                $http.post(url, json,{cache: false}).then(function(response) {
                    callback(response);
                });               
            }; 

             ClienteService.prototype.deletar= function (json, callback) { 
                var url = "http://localhost/emervet/api/index.php/cliente/deletar/";            
                var contentType = {'Content-Type': 'application/json'};
                $http.post(url, json,{cache: false}).then(function(response) {
                    callback(response);
                });               
            };

            ClienteService.prototype.savePacienteByCliente= function (json, clienteid, callback) {
                var url = "http://localhost/emervet/api/index.php/cliente/savePacienteByCliente/"+clienteid;            
                var contentType = {'Content-Type': 'application/json'};
                $http.post(url, json,{cache: false}).then(function(response) {
                    callback(response);
                });               
            }; 
            return ClienteService;
        }])

}) (); 


