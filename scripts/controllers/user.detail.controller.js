(function () {
    'use strict';
    angular.module('app')
        .controller('userDetailCtrl', ['$scope', '$http', '$rootScope', '$location','ClienteService', '$timeout', userDetailCtrl])        

    function userDetailCtrl($scope, $http, $rootScope, $location,ClienteService,$timeout) {        
        
        $scope.init = function(){
            $scope.service = new ClienteService($scope);
            $scope.getCliente();
            $scope.mostrar_alerta = false;
            $scope.pacientes_array = [];
            
          //  $scope.imageService = new ImageService($scope);
           
        }    



        $scope.getCliente = function(){ 
            var url = $location.path(); 
            $scope.idp = url.substring(url.lastIndexOf('/') + 1);
            $scope.qtd_paciente = 0;

            if(!isNaN($scope.idp)){ 
            
                $scope.service.getClienteById($scope.idp, function (result){ 
                   $scope.cliente = result.data[0];  
                   $scope.service.getPacienteByCliente($scope.idp, function (result2){ 

                        $scope.pacientes = result2.data; 
                        for(var i = 0; i<$scope.pacientes.length; i++)
                            $scope.pacientes[i].peso = parseFloat($scope.pacientes[i].peso);

                        
                        $scope.qtd_paciente = $scope.pacientes.length;                 
                    })           
                })
                // alert(JSON.stringify($scope.pacientes))
              
            }
        


            //alert(JSON.stringify($scope.cliente))
        }

        $scope.addpaciente = function(){  
            if(isNaN($scope.idp)){
               $scope.service.save($scope.cliente, function (result){         
                    $location.path('paciente-cliente/' + result.data.data);
               })
            }else{ 
                $location.path('paciente-cliente/' + $scope.idp);
            }
        }


        $scope.salvar = function(){
            if(!isNaN($scope.idp)){ 
                var arraypacientes = [];            
                arraypacientes = $scope.pacientes;
                $scope.cliente.pacientes = arraypacientes;
            }else{
                $scope.pacientes_array.push($scope.pacientes);
                $scope.cliente.pacientes = $scope.pacientes_array;
            }
           
            $scope.service.save($scope.cliente, function (result){ 
                    $scope.mostrar_alerta = true;
                    

                    $timeout($scope.mudarpagina, 2000);

                       
            })           
        }

        $scope.mudarpagina = function(){
           
            $location.path('clientes')
        }

       
       

        $scope.init();
        
    };


})(); 