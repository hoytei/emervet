(function () {
    'use strict';
    angular.module('app')
        .controller('medicamentoCtrl', ['$scope', '$http', '$rootScope', '$location','MedicamentoService','$timeout', medicamentoCtrl])        

    function medicamentoCtrl($scope, $http, $rootScope, $location, MedicamentoService,$timeout) {        
        
        $scope.init = function(){
            $scope.service = new MedicamentoService($scope);
            $scope.getMedicamentos();
          
           
        }

        $scope.getMedicamentos = function(){
            $scope.service.getMedicamentos(function (result){ 
               $scope.medicamentos= result.data;
               
            })
           
        }


        $scope.salvar = function(){
            alert('salvo')
        }

        $scope.goTo = function(medicamento) { 
            $location.path('medicamento-detalhe/' + medicamento.MedicamentoID);
        };

         $scope.deletar = function(item){
            $scope.service.deletar(item.MedicamentoID, function (result){
               $scope.deletado = true;
               $timeout($scope.mostrarfalse, 2000);
               $scope.getMedicamentos();
            })
        }


        $scope.mostrarfalse = function(){
            $scope.deletado = false;
        }

       
       

        $scope.init();
        
    };


})(); 