(function () {
    'use strict';
    angular.module('app')
        .controller('pacienteClienteDetailCtrl', ['$scope', '$http', '$rootScope', '$location','ClienteService', '$timeout', pacienteClienteDetailCtrl])        

    function pacienteClienteDetailCtrl($scope, $http, $rootScope, $location,ClienteService,$timeout) {        
        
        $scope.init = function(){
            $scope.service = new ClienteService($scope);
            var url = $location.path(); 
            $scope.clienteid = url.substring(url.lastIndexOf('/') + 1);   
            $scope.especies = [{
                                nome: 'Canino'
                              },
                              {
                                nome: 'Felino'
                              }]       
           
        }

       
        $scope.salvar = function(){
            $scope.paciente.especie = $scope.selected_especie.nome;
            $scope.service.savePacienteByCliente($scope.paciente, $scope.clienteid, function (result){ 
                     $location.path('cliente-detalhe/' + $scope.clienteid);                       
            })           
        }

        

       
       

        $scope.init();
        
    };


})(); 