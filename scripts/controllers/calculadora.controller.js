(function () {
    'use strict';
    angular.module('app')
        .controller('calculadoraCtrl', ['$scope', '$http', '$rootScope', '$location','MedicamentoService', calculadoraCtrl])        

    function calculadoraCtrl($scope, $http, $rootScope, $location, MedicamentoService) {        
        
        $scope.init = function(){
            $scope.service = new MedicamentoService($scope);
            $scope.getMedicamentos();
            $scope.especies = [{
                                nome: 'Canino'
                              },
                              {
                                nome: 'Felino'
                              }]
           
        }

        $scope.getMedicamentos = function(){
            $scope.service.getMedicamentos(function (result){ 
               $scope.medicamentos= result.data;
               
            })
           
        }


        $scope.calcular = function(){
         
            if($scope.selected_especie.nome = 'Canino'){
                 $scope.resultado = ($scope.selected.DoseCao * $scope.peso)/$scope.selected.Concentracao + " ml";

            }else{
                 $scope.resultado = ($scope.selected.DoseGato * $scope.peso)/$scope.selected.Concentracao + " ml";
            }    
        }


       
       

        $scope.init();
        
    };


})(); 