(function () {
    'use strict';
    angular.module('app')
        .controller('pacienteDetailCtrl', ['$scope', '$http', '$rootScope', '$location','PacienteService', 'MedicamentoService','$window','$timeout', pacienteDetailCtrl])        

    function pacienteDetailCtrl($scope, $http, $rootScope, $location, PacienteService, MedicamentoService,$window, $timeout) {        
        
        $scope.init = function(){
            $scope.service = new PacienteService($scope);
            $scope.medservice = new MedicamentoService($scope);
           
            $scope.loadPaciente(); 
            $scope.loadMedicamentos();
        }

         function formatDate (input) {
          var datePart = input.match(/\d+/g),
          year = datePart[0].substring(2), // get only two digits
          month = datePart[1], day = datePart[2];

          return day+'/'+month+'/'+year;
        }

        $scope.loadPaciente = function(){ 
             var url = $location.path(); 
            $scope.idpaciente = url.substring(url.lastIndexOf('/') + 1);  
            $scope.service.getPacienteById($scope.idpaciente,function (result){  
                $scope.paciente = result.data[0];   
                $scope.paciente.peso = parseFloat($scope.paciente.peso);                  
            })

            $scope.service.getMedicamentoByPaciente($scope.idpaciente,function (result){  
               
                $scope.lista_medicamentos = result.data;     

                for(var i = 0; i<$scope.lista_medicamentos.length; i++ ){
                    $scope.lista_medicamentos[i].data = formatDate($scope.lista_medicamentos[i].data)
                }                
            })

           
            
        }

        $scope.loadMedicamentos = function(){ 
             
            $scope.medservice.getMedicamentos(function (result){  
                $scope.medicamentos = result.data;    
                                 
            })

          
        }

          $scope.calculadose = function(medicamento){ 

            if($scope.paciente.especie == "Canino"){
            
                $scope.dose =  (medicamento.DoseCao * $scope.paciente.peso)/medicamento.Concentracao;
            }else{
                $scope.dose =  (medicamento.DoseGato * $scope.paciente.peso)/medicamento.Concentracao;
 
            }
        }



        $scope.addmedicamento = function(){  
            $scope.novo_pm = {
                    pacienteid: $scope.idpaciente,
                    medicamentoid: $scope.selected.MedicamentoID,
                    doseutilizada: $scope.dose
            }
           
            $scope.medservice.savePacienteMedicamento($scope.novo_pm, function (result){  

             $window.location.reload()
                                 
            })

          
        }

       

        $scope.getPaciente = function(){ 
           
            
         

            if(!isNaN(id)){          
                for(var i = 0; i < $scope.pacientes.length; i++){
                    if($scope.pacientes[i].id == id){
                        $scope.paciente = $scope.pacientes[i];
                    }
                }
               
            }
        
           
        }


        $scope.salvar = function(){
             $scope.service.save($scope.paciente,function (result){  
                 $scope.mostrar_alerta = true;
                    

                    $timeout($scope.mudarpagina, 2000);

                       
            })           
        }

        $scope.mudarpagina = function(){
           
            $location.path('pacientes')
        }

        

       
       

        $scope.init();
        
    };


})(); 