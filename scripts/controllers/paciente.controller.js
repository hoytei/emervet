(function () {
    'use strict';
    angular.module('app')
        .controller('pacienteCtrl', ['$scope', '$http', '$rootScope', '$location','PacienteService','$timeout', pacienteCtrl])        

    function pacienteCtrl($scope, $http, $rootScope, $location, PacienteService,$timeout) {        
        
        $scope.init = function(){
            $scope.service = new PacienteService($scope);
            $scope.getPaciente();         
           
        }

        $scope.getPaciente = function(){
            $scope.service.getPacientes(function (result){ 
                $scope.pacientes = result.data; 
            })
            
        }



        $scope.goTo = function(paciente) { 
            $location.path('paciente-detalhe/' + paciente.pacienteid);
        };

         $scope.deletar = function(item){
            $scope.service.deletar(item.pacienteid, function (result){
               $scope.deletado = true;
               $timeout($scope.mostrarfalse, 2000);
               $scope.getPaciente();
            })
        }

        $scope.mostrarfalse = function(){
            $scope.deletado = false;
        }

       
       

        $scope.init();
        
    };


})(); 