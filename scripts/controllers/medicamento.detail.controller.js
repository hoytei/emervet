(function () {
    'use strict';
    angular.module('app')
        .controller('medicamentoDetailCtrl', ['$scope', '$http', '$rootScope', '$location','MedicamentoService','$timeout', medicamentoDetailCtrl])        

    function medicamentoDetailCtrl($scope, $http, $rootScope, $location,MedicamentoService,$timeout) {        
        
        $scope.init = function(){
            $scope.service = new MedicamentoService($scope);
            $scope.loadClientes();
            $scope.tipos = [{ nome: 'mg'},
                            { nome: 'ml'}];
         
           
        }

        $scope.loadClientes = function(){
            var url = $location.path(); 
            $scope.medicamentoid = url.substring(url.lastIndexOf('/') + 1);
            if(isNaN($scope.medicamentoid)){  
               $scope.service.getMedicamentoById($scope.medicamentoid, function (result){ 
                   $scope.medicamento = result.data[0];

                   
                 
                })
            }

                    
        }



        $scope.salvar = function(){
           
             $scope.service.save($scope.medicamento,function (result){  
                 $scope.mostrar_alerta = true;
                 $timeout($scope.mudarpagina, 2000);

                       
            })           
        }

        $scope.mudarpagina = function(){
           
            $location.path('medicamentos')
        }

       
       

        $scope.init();
        
    };


})(); 