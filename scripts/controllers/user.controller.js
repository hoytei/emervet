(function () {
    'use strict';
    angular.module('app')
        .controller('userCtrl', ['$scope', '$http', '$rootScope', '$location','ClienteService', '$timeout', userCtrl])        

    function userCtrl($scope, $http, $rootScope, $location, ClienteService, $timeout) {        
        
        $scope.init = function(){
            $scope.service = new ClienteService($scope);
            $scope.getCliente();
          
           
        }

        $scope.getCliente = function(){
            $scope.service.getClientes(function (result){ 
               $scope.clientes = result.data;
            })
           
        }



        $scope.goTo = function(cliente) {
            $location.path('cliente-detalhe/' + cliente.clienteid);
        };

        $scope.deletar = function(item){
            $scope.service.deletar(item.clienteid, function (result){
               $scope.deletado = true;
               $timeout($scope.mostrarfalse, 2000);
               $scope.getCliente();
            })
        }

        $scope.mostrarfalse = function(){
            $scope.deletado = false;
        }

       
       

        $scope.init();
        
    };


})(); 